# Generated by Django 4.0.6 on 2022-07-22 10:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api_user', '0003_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='User',
        ),
    ]
