# from django.http import HttpRequest, HttpResponse
# from django.shortcuts import render, redirect
# from django.contrib import messages
# from django.contrib.auth import authenticate, login
# from django.contrib.auth.decorators import login_required
# from django.contrib.auth import authenticate, login, logout  
# from .forms import CreateUserForm, UserCreationForm
# from django.core.mail import send_mail
# from django.core.mail import EmailMultiAlternatives
# from django.template.loader import get_template
# from django.template import Context

   
#################### index####################################### 
# def index(request):
#     return render(request, 'api_user/index.html')
   
########### register here ##################################### 
# def register(request):
#     if request.method == 'POST':
#         form = UserRegisterForm(request.POST)
#         if form.is_valid():
#             form.save()
#             username = form.cleaned_data.get('username')
#             email = form.cleaned_data.get('email')
#             ######################### mail system #################################### 
#             htmly = get_template('api_user/email.html')
#             d = { 'username': username }
#             subject, from_email, to = 'welcome', 'your_email@gmail.com', email
#             html_content = htmly.render(d)
#             msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
#             msg.attach_alternative(html_content, "text/html")
#             msg.send()
#             ################################################################## 
#             messages.success(request, f'Your account has been created ! You are now able to log in')
#             return redirect('login')
#     else:
#         form = UserRegisterForm()
#     return render(request, 'api_user/register.html', {'form': form, 'title':'reqister here'})
   
################ login forms################################################### 
# def Login(request):
#     if request.method == 'POST':
   
#         # AuthenticationForm_can_also_be_used__
   
#         username = request.POST['username']
#         password = request.POST['password']
#         user = authenticate(request, username = username, password = password)
#         if user is not None:
#             form = login(request, user)
#             messages.success(request, f' wecome {username} !!')
#             return redirect('index')
#         else:
#             messages.info(request, f'account done not exit plz sign in')
#     form = AuthenticationForm()
#     return render(request, 'api_user/login.html', {'form':form, 'title':'log in'})

# def register(request):
#     if request.method == 'POST':
#         form = UserRegisterForm(request.POST)
#         if form.is_valid():
#             form.save()
#             user = form.cleaned_data.get('username')
#             messages.success(request,'Account was created for' + user )
#             return redirect('login')
    
#     else:
#         form = UserRegisterForm()
#     return render(request, 'api_user/reg.html', {'form': form})

# def Login(request):
#     if request.method == 'POST':
#         username = request.POST.get('username')
#         password = request.POST.get('password')
#         user = authenticate(request, username=username, password=password)
#         if user is not None:
#             login(request, user)
#             return redirect('home')

#     return render(request, 'api_user/login.html')

# def home(request):
#     return HttpResponse('Welcome to Home Page......')



from fileinput import filename
from multiprocessing import context
from urllib import response
from django.conf import settings
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
import requests
from api_user.models import FilesAdmin

# from api_project.api_user import serializers, RegSerilizer
from .forms import UserRegisterForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.authtoken.serializers import AuthTokenSerializer
from knox.auth import AuthToken
from . serializers import RegSerilizer
from rest_framework.views import APIView
from rest_framework.parsers import FileUploadParser
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
import os
from django.http import Http404, HttpResponse
from . models import FilesAdmin, FileHandler, UploadFile
from django.contrib.auth.models import User,auth
from . forms import FileHandlerForm
from django.views.generic import TemplateView



def home(request):
    return render(request, 'api_user/home.html')


def register(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Hi {username}, your account was created successfully')
            return redirect('login')
    else:
        form = UserRegisterForm()

    return render(request, 'api_user/register.html', {'form': form})


@login_required()
def profile(request):
    return render(request, 'api_user/profile.html')

@api_view(['POST'])
def login_api(request):
    serializer = AuthTokenSerializer(data=request.data)
    print(request.data)
    serializer.is_valid(raise_exception=True)
    user = serializer.validated_data['user']
    print(user)

    _, token = AuthToken.objects.create(user)
    return Response({
            'user_info':{
                'id':user.id,
                'username':user.username,
                'email':user.email
            },
            'token':token
        })

@api_view(['GET'])
def get_user_data(request):
    user = request.user
    print(request.user)

    if user.is_authenticated:
        return Response({
            'user_info':{
                'id':user.id,
                'username':user.username,
                'email':user.email
            },
        })
    return Response({'error':'Not Authenticated'}, status=400)


@api_view(['POST'])
def reg_api(request):
    serializer = RegSerilizer(data=request.data)
    serializer.is_valid(raise_exception=True)

    user = serializer.save()
    _, token = AuthToken.objects.create(user)

    return Response({
            'user_info':{
                'id':user.id,
                'username':user.username,
                'email':user.email
            },
            'token':token
    })


class UploadView(APIView):
    parser_classes = (FileUploadParser,)
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        file = request.data.get('file', None)
        import pdb; pdb.set_trace()
        print(file)
        if file:
            return Response({'message':'File Received'}, status=200)
        else:
            return Response({'message':'File Missing'}, status=400)

def homes(request):
    context = { 'files':FilesAdmin.objects.all() }
    return render(request, 'api_user/upload.html',context)

# def download(request,path):
#     file_path=os.path.join(settings.MEDIA_ROOT, path)
#     print(file_path)
#     if os.path.exists(file_path):
#         with open(file_path,'rb') as fh:
#             response = HttpResponse(fh.read(),content_type="application/pdf")
#             print(response)
#             response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
#             return response
#     raise Http404        

def download(request, path):
    file_path = os.path.join(settings.MEDIA_ROOT, path)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404

class FileView(TemplateView):

    template_name = 'api_user/files.html'

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        get_files = FileHandler.objects.all()
        context = { 'form': FileHandlerForm, 'get_file': get_files }
        return context

    def post(self,request):
        context = {}
        if request.method == "POST":
            form = FileHandlerForm(request.POST,request.FILES)

            if form.is_valid():
                FileHandler.objects.get_or_create(fileupload = form.cleaned_data.get('fileupload'))

                return redirect('/files')
            else:
                context['form'] = form
        else:
            context['form'] = form
        
        return redirect('/files')


def index(request):
    return render(request,'api_user/ind.html')
def index1(request):
    if request.method=='POST':
        title=request.POST['title']       
        upload1=request.FILES['upload']
        object=UploadFile.objects.create(title=title,upload=upload1)
        object.save()  
    context=UploadFile.objects.all()
    return render(request,'api_user/index1.html',{'context':context})



import mimetypes

def download_file(request):
    # Define Django project base directory
    # BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # Define text file name
    # filename = 'json.txt'
    filename = 'Suriya.xlsx'
    print(filename)
    # Define the full file path
    # filepath = BASE_DIR + '/Documents/' + filename
    # filepath = '/home/suriya/Documents/' + filename
    filepath = '/home/suriya/Downloads/' + filename

    # Open the file for reading content
    path = open(filepath, 'rb')
    # Set the mime type
    mime_type, _ = mimetypes.guess_type(filepath)
    # Set the return value of the HttpResponse
    response = HttpResponse(path, content_type=mime_type)
    # Set the HTTP header for sending to browser
    response['Content-Disposition'] = "attachment; filename=%s" % filename
    # Return the response value
    return response
    # return Response({'message':'File Received'}, status=200)


# import mimetypes

# def download_file(request):
#     filename = 'Suriya.xlsx'
#     print(filename)
#     filepath = '/home/suriya/Downloads/' + filename
#     path = open(filepath, 'rb')
#     mime_type, _ = mimetypes.guess_type(filepath)
#     response = HttpResponse(path, content_type=mime_type)
#     response['Content-Disposition'] = "attachment; filename=%s" % filename
#     return response
