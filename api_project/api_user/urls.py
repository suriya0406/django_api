# from django.contrib import admin
# from django.urls import path
# from api_user import views as user_view
# from django.contrib.auth import views as auth

# urlpatterns = [

#     ##### user related path########################## 
#     path('', user_view.index, name ='index'),
#     path('login/', user_view.Login, name ='login'),
#     path('logout/', auth.LogoutView.as_view(template_name ='api_user/index.html'), name ='logout'),
#     path('register/', user_view.register, name ='register'),

  
# ]




from django.urls import path
from . import views
from .  views import FileView, UploadView

from django.contrib.auth import views as auth_view
from knox import views as knox_views
from django.conf import settings
from django.conf.urls.static import static
# from django.conf.urls import url
from django.urls import re_path as url
from django.views.static import serve


urlpatterns = [
    path('', views.home, name='home'),
    path('register/', views.register, name='register'),
    path('profile/', views.profile, name='profile'),
    path('login/', auth_view.LoginView.as_view(template_name='api_user/log.html'), name="login"),
    path('logout/', auth_view.LogoutView.as_view(template_name='api_user/logout.html'), name="logout"),
    path('login_api/', views.login_api, name='login_api'),
    path('get_user_data/', views.get_user_data, name='get_user_data'),
    path('reg_api/', views.reg_api, name='reg_api'),
    path('logout_api/', knox_views.LogoutView.as_view(), name='logout'),
    path('logoutall_api/', knox_views.LogoutAllView.as_view(), name='logoutall'),
    path('upload/', UploadView.as_view(), name='upload'),
    path('upload_file/',views.homes),
    path('download_file/', views.download_file),

    url(r'^download/(?P<path>.*)$',serve,{'document_root':settings.MEDIA_ROOT}),
    # url(r'^static/(?P<path>.*)$',serve, {'document_root': settings.MEDIA_ROOT}),
    path('files/', FileView.as_view(), name='files'),
    path("index/",views.index,name="index"),
    path("index1/",views.index1,name="index1"),

    


]

if settings.DEBUG:
    # urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)


