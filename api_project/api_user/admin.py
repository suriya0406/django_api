from django.contrib import admin
from . models import FilesAdmin, FileHandler

# Register your models here.

admin.site.register(FilesAdmin)
admin.site.register(FileHandler)
