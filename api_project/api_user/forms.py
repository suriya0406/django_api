from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import FileHandler
  
  
class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

   
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
        

# from django.contrib.auth.models import User
# from django.contrib.auth.forms import UserCreationForm
# from django import forms


# class UserRegisterForm(UserCreationForm):
#     email = forms.EmailField()

#     class Meta:
#         model = User
#         fields = ['username', 'email', 'password1', 'password2']

class FileHandlerForm(forms.ModelForm):
    fileupload = forms.FileField()

    class Meta():
        model = FileHandler
        fields = ['fileupload']
        
