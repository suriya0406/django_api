from django.db import models
import os


class FilesAdmin(models.Model):
    adminupload = models.FileField(upload_to='media')
    title = models.CharField(max_length=50)

    def __str__(self):
        return str(self.title)

def file_path(instance, filename):
    # path = "documents/"
    format = "uploaded/" + filename
    return os.path.join( format)

class FileHandler(models.Model):
    fileupload = models.FileField(upload_to=file_path)

    def __str__(self):
        return str(self.fileupload)


    
class UploadFile(models.Model):
    title=models.CharField(max_length=50)
    upload=models.FileField(upload_to="media")
    