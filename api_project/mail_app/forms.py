from django import forms

from mail_app.models import Dashboard

class Subscribe(forms.Form):
    Email = forms.EmailField()
    def __str__(self):
        return self.Email 

class LoginForm(forms.ModelForm):  

    class Meta:  
        model = Dashboard  
        fields = "__all__"  