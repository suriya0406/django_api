from unicodedata import name
from django.db import models

class Dashboard(models.Model):
    Meeting_link=models.CharField(max_length=30)
    Time=models.CharField(max_length=30)
    Email=models.CharField(max_length=30)

    class Meta:
        db_table="Dashboard"