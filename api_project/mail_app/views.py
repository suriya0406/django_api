from syslog import LOG_INFO
from django.shortcuts import render, redirect, HttpResponseRedirect
from django.http import HttpResponse
from api_project.settings import EMAIL_HOST_USER
from mail_app.forms import LoginForm, Subscribe

from django.core.mail import send_mail
import requests
from mail_app.serializers import DashboardSerializer
from mail_app.models import Dashboard
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated




# DataFlair #Send Email
def subscribe(request):
    sub = Subscribe()
    if request.method == 'POST':
        sub = Subscribe(request.POST)
        subject = 'Welcome to DataFlair'
        message = 'Hope you are enjoying your Django Tutorials'
        recepient = str(sub['Email'].value())
        send_mail(subject, 
            message, EMAIL_HOST_USER, [recepient], fail_silently = False)
        return render(request, 'mail_app/success.html', {'recepient': recepient})
    return render(request, 'mail_app/email.html', {'form':sub})

 
# def subscribe(request):  
#     subject = "Greetings"  
#     msg     = "Congratulations for your success"  
#     recepient      = "ramdhanushv@gmail.com"
#     res     = send_mail(subject, msg,EMAIL_HOST_USER, [recepient], fail_silently = False)  
#     if(res == 1):  
#         msg = "Mail Sent Successfuly"  
#     else:  
#         msg = "Mail could not sent"  
#     return HttpResponse(msg)  


# class Dashboard():
def login(request):
    form = LoginForm()
    return render(request,'dash.html',{'form':form})

def show(request):
    headers = {'Content-Type': 'application/json'}
    if request.method == "POST":
        form = LoginForm(request.POST) 
        if form.is_valid():
            form.save()
            # return redirect('/list')
            # post_data = {'Meeting_link': form.cleaned_data['Meeting_link'], 'Time': form.cleaned_data['Time'],'Email': form.cleaned_data['Email']}
            # response = requests.post('http://127.0.0.1:8000/list/', headers=headers, data=post_data)
            # return HttpResponse('/list')


    # data=Dashboard.objects.all()
    # return render(request,'list.html',{'form':data})  

    
def list(request):  
    data=Dashboard.objects.all()
    return render(request,'list.html',{'form':data})
    # meet_link=request.POST.get('Meeting_link') 
    # time=request.POST.get('time') 
    # email=request.POST.get('email') 


class HelloView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        content = {'message': 'Hello, World!'}
        return Response(content)