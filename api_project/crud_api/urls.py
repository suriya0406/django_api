from django.urls import path
from .views import EmployeeCreateApi, EmployeeApi, EmployeeUpdateApi, EmployeeDeleteApi, UserApi
from crud_api import views


urlpatterns = [
    path('create/',EmployeeCreateApi.as_view()),
    path('list/',EmployeeApi.as_view()),
    path('update/<int:pk>',EmployeeUpdateApi.as_view()),
    path('delete/<int:pk>/',EmployeeDeleteApi.as_view()),
    path('orm/',views.orm,name='orm'),
    path('register/',views.register,name='register'),
    path('user_list/',views.user_list,name='user_list'),
    path('user_login/',views.user_login,name='user_login'),
    path('register_user/',UserApi.as_view()),
    # path('create_user/',CreateUserView.as_view()),
    # path('user/',views.create_auth,name='user'),



]
  