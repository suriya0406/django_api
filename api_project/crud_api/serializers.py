from rest_framework.serializers import ModelSerializer
from .models import Employee, User
from rest_framework import serializers



class EmployeeSerializer(ModelSerializer):
    
    class Meta:
        model=Employee

        fields=[ 'Emp_id','Emp_Name','Emp_City','Emp_Phone','Emp_Email'

        ]

class UserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(style={
        'input_type': 'password', 'placeholder': 'Password'
        })
    
    class Meta:
        model = User
        fields = ['first_name', 'last_name','user_name','email','password','phone']
    

