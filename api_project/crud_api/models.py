from django.db import models
from django.conf import settings
from django.contrib.auth.models import User




class Employee(models.Model):
    Emp_id=models.CharField(max_length=30)
    Emp_Name=models.CharField(max_length=30)
    Emp_City=models.CharField(max_length=30)
    Emp_Phone=models.CharField(max_length=30)
    Emp_Email=models.CharField(max_length=30)

class User(models.Model):

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    user_name = models.CharField(max_length=100)
    email = models.EmailField()
    password = models.CharField(max_length=50)
    phone = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True,
                                  editable=False)
    updated_at = models.DateTimeField(auto_now=True,
                                  editable=False)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, related_name='user_created_by', null=True, default=None)

    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, related_name='user_updated_by', null=True, default=None)
    class Meta:
        db_table = 'users'
