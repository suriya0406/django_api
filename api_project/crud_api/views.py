
# from django.shortcuts import render
# from rest_framework.views import APIView
# from .models import Employee
# from .serializers import EmployeeSerializer
# Create your views here.

# class ContactAPI(ListCreateAPIView):

#     serializer_class=EmployeeSerializer
#     permission_classes=(permissions.IsAdminUser,)
    

#     def create(self, serializer):
#         serializer.save()
    
#     def get_queryset(self):
#         return Employee.objects.all()


from django.http import HttpResponse, JsonResponse
from django.urls import is_valid_path
from requests import request
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveUpdateAPIView, DestroyAPIView
from rest_framework.response import Response

from .serializers import EmployeeSerializer, UserSerializer
from .models import Employee, User
from django.shortcuts import render
from django.conf import settings
from crud_api.pagination import CustomPageNumberPagination
from knox.auth import AuthToken
from rest_framework.decorators import api_view
from django.contrib.auth.hashers import make_password, check_password
from rest_framework import status
from rest_framework.authtoken.serializers import AuthTokenSerializer





class EmployeeCreateApi(CreateAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

class EmployeeApi(ListAPIView):
    queryset = Employee.objects.all()
    pagination_class = CustomPageNumberPagination

    serializer_class = EmployeeSerializer

class EmployeeUpdateApi(RetrieveUpdateAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

class EmployeeDeleteApi(DestroyAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

def orm(request):
    # queryset=Employee.objects.get().values()
    # queryset = Employee.objects.filter(Emp_Name__startswith='r').values()
    queryset = Employee.objects.filter(Emp_Name__contains='Suriya A').values()
    
    # queryset = Employee.objects.all().order_by('-id').values()

    # RAW QUERY 

    # queryset=Employee.objects.raw('select * from crud_api_employee where id = 1').query

    # print(orm)
    return HttpResponse(queryset)


@api_view(['POST'])
def register(request):
#     user = UserModel.objects.create(
#     first_name=validated_data['first_name'],
#     last_name=validated_data['last_name'],
#     email=validated_data['email'],
#     password = make_password(validated_data['password'])
# )
    serializer = UserSerializer(data=request.data)
    # print(make_password(request.POST.get('password')))
    serializer.is_valid(raise_exception=True)
    user = serializer
    # print(user)
    # print(make_password(request.POST.get('password')))
    # pwd=make_password(request.POST.get('password'))
    user.save()
    # _, token = AuthToken.objects.create(user)

    return Response({
            'user_info':{
                'id':user.id,
                'first_name':user.first_name,
                'last_name':user.last_name,
                'email':user.email,
                'phone':user.phone
            },
            # 'token':token
    })

class UserApi(CreateAPIView):

    queryset = User.objects.all()
    serializer_class = UserSerializer

@api_view(['GET', 'POST'])
def user_list(request):
  
    if request.method == 'GET':
        user = User.objects.all()
        serializer = UserSerializer(user, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        data = {
            'first_name': request.data['first_name'],
            'last_name'  : request.data['last_name'],
            'user_name'  : request.data['user_name'],
            'email'    : request.data['email'],
            'password'    :make_password(request.data['password']),
            'phone'     :request.data['phone']
        }         
        serializer = UserSerializer(data=data)
        if serializer.is_valid():  
            serializer.save()
            return Response(serializer.data , status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

api_view(['GET','POST'])
def user_login(request):
    if request.method == "GET":
        user = User.objects.filter(user_name = 'surya@123').values()
        serializer = UserSerializer(data=user)
        print(serializer.is_valid())

        if serializer.is_valid():
            serializer.password = check_password(serializer.data['password'])
            print(serializer)
        # print(serializer.data['password'])
    # if user.password(request.GET['password']):
    #     print(user.values())
    # user_name = request.GET['user_name']
    # password = request.GET['password']
    # print(user_name,password)
    return HttpResponse(user)
