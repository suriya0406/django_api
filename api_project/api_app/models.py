from pyexpat import model
from django.db import models

# Create your models here.

class User(models.Model):
    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)

class JoinTableModel(models.Model):
    id=models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    email = models.CharField(max_length=200)

