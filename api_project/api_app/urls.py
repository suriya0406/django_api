from xml.etree.ElementInclude import include
from django.db import router
from django.urls import path, include
from . import views
from .views import UserView, JoinTableAPI
from rest_framework.routers import DefaultRouter
from django.conf import settings
from django.conf.urls.static import static

router =  DefaultRouter()
router.register('JoinTableAPI/', views.JoinTableAPI),

urlpatterns = [
    path('user/', UserView.as_view()),
    path('user/<int:pk>', UserView.as_view()), # to capture our ids
    path('array/', views.array),
    path('dict/', views.dict),
    path('json/', views.json_array),
    path('raw_query/', views.raw_query),
    path('join_table/', include(router.urls)),
    path('display_join_table/', views.display_join_table),
    path('files/', views.file,name='files'),
    path('multiple_files/', views.file,name='multiple_files'),

    
    
]

if settings.DEBUG:
    # urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
