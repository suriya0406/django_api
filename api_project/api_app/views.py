from django.db import connection
from django.http import HttpResponse, JsonResponse
from django.http.response import Http404
from django.shortcuts import render
from rest_framework.views import APIView
from .models import  User, JoinTableModel
from .serializers import UserSerializer, JoinTableSerializers
from rest_framework.response import Response
from django.core import serializers
from urllib.request import urlopen
import json
import requests
from rest_framework import viewsets


class UserView(APIView):

    # READ a single Todo
    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk=None):
        if pk:
            data = self.get_object(pk)
        else:
            data = User.objects.all()

        serializer = UserSerializer(data, many=True)

        return Response(serializer.data)

    def post(self, request):
        data = request.data
        serializer = UserSerializer(data=data)

        serializer.is_valid(raise_exception=True)

        serializer.save()

        response = Response()

        response.data = {
            'message': 'User Created Successfully',
            'data': serializer.data
        }

        return response

    def post(self, request, pk=None):
        user_to_update = User.objects.get(pk=pk)
        serializer = UserSerializer(instance=user_to_update,data=request.data, partial=True)

        serializer.is_valid(raise_exception=True)

        serializer.save()

        response = Response()

        response.data = {
            'message': 'User Updated Successfully',
            'data': serializer.data
        }

        return response

    def delete(self, request, pk):
        user_to_delete =  User.objects.get(pk=pk)

        user_to_delete.delete()

        return Response({
            'message': 'User Deleted Successfully'
        })

def array(request):
    arr=[]
    user=User.objects.all()
    user_serial=UserSerializer(user,many=True)
    # people = serializers.serialize("json", User.objects.all())
    people = User.objects.all().values()

    for arrays in user_serial.data:
        res={'Namesss':arrays['name'],'Cityss':arrays['city'],'Phoneesss':arrays['phone']}
        arr.append(res)
    return HttpResponse( people)

def dict(request):
    arr=[]
    dictionary = {
        'name':'suriya',
        'city':'cbe'
    }
    arr.append(dictionary)
    dictionary={
        'name':'ram',
        'city':'eorde'
    }

    # print dictionary
    # for i in dictionary:
    arr.append(dictionary)
    arr.pop(1)
    return HttpResponse(arr)

def json_array(request):
    array=[]
    # text_file = open("/home/suriya/Documents/json.txt", "r")
    # text_file = open("/home/suriya/Downloads/Item Catalog Pages (old).json", "r")
    text_file = open("/home/suriya/Downloads/json.json", "r")

    # lines = text_file.read().split(',')
    data=json.load(text_file)
    # i=0
    for arr in data:
        res={'sku_id':arr['SKU'],'catalog':arr['Catalog'],'year':arr['Year'],'page_no':arr['Page_Number']}
        array.append(res)
    return HttpResponse(array)

def raw_query(request):
    sql = 'SELECT  user.id, user.name, user.city, Dashboard.email FROM api_app_user as user INNER JOIN Dashboard ON user.id = Dashboard.id'
    user=User.objects.raw(sql)
    print(user)
    print(connection.queries)
    return render(request,'api_app/output.html',{'data': user})
    # user = User.objects.all()
    # var =[]
    # for obj in user:
    #     print (obj.id, obj.name, obj.city)
    #     data = {'id':obj.id, 'name': obj.name}
    #     var.append(data)
    # return HttpResponse(var)

class JoinTableAPI(viewsets.ModelViewSet):
    queryset = JoinTableModel.objects.raw('CALL JoinTable() ')
    serializer_class = JoinTableSerializers

def display_join_table(request):
    jsontable = requests.get('http://127.0.0.1:8000/api/join_table/JoinTableAPI//')
    JsonObj = jsontable.json()
    return render(request, 'api_app/join_table.html',{'JsonObj':JsonObj})


def file(request):
    if request.method == "GET":
        print('hai')
        return render(request,'api_app/multiple_files.html')

# def multiple_files(request):
    if request.method == "POST":
        files = request.FILES.getlist('files[]', None)
        print(files)
        for f in files:
            handle_uploaded_file(f)
        return JsonResponse({'msg':'<div class="alert alert-success" role="alert"> File Uploaded Successfully</div>'})
    else:
        return render(request,'api_app/multiple_files.html')

def handle_uploaded_file(f):
    with open('api_app/static/upload/'+f.name,'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


