from django.http.response import Http404
from django.shortcuts import render
from rest_framework.views import APIView
from .models import  User
from .serializers import UserSerializer
from rest_framework.response import Response

class UserView(APIView):

    # READ a single Todo
    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk=None, format=None):
        if pk:
            data = self.get_object(pk)
        else:
            data = User.objects.all()

        serializer = UserSerializer(data, many=True)

        return Response(serializer.data)

    def post(self, request, format=None):
        data = request.data
        serializer = UserSerializer(data=data)

        serializer.is_valid(raise_exception=True)

        serializer.save()

        response = Response()

        response.data = {
            'message': 'User Created Successfully',
            'data': serializer.data
        }

        return response

    def put(self, request, pk=None, format=None):
        user_to_update = User.objects.get(pk=pk)
        serializer = UserSerializer(instance=user_to_update,data=request.data, partial=True)

        serializer.is_valid(raise_exception=True)

        serializer.save()

        response = Response()

        response.data = {
            'message': 'User Updated Successfully',
            'data': serializer.data
        }

        return response

    def delete(self, request, pk, format=None):
        user_to_delete =  User.objects.get(pk=pk)

        user_to_delete.delete()

        return Response({
            'message': 'User Deleted Successfully'
        })