from django.urls import path
from .views import User

urlpatterns = [
    path('cart-items/', User.as_view())
]