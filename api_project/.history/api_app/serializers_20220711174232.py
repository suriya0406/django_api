from rest_framework import serializers
from .models import User

class UserSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=200)
    city = serializers.CharField(max_length=200)
    phone = serializers.CharField(max_length=200)


    class Meta:
        model = CartItem
        fields = ('__all__')