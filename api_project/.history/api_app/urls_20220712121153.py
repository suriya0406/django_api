from django.urls import path
from .views import UserView

urlpatterns = [
    path('user/', UserView.as_view()),
    path('user/<:pk>', UserView.as_view()) # to capture our ids

]
