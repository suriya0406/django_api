from django.urls import path
from .views import UserView

urlpatterns = [
    path('user/', User.as_view())
]