from django.urls import path
from .views import User

urlpatterns = [
    path('cart-items/', CartItemViews.as_view())
]